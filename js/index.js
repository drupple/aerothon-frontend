$(document).ready(function(){
  $('.modal').modal();
  $('select').formSelect();
  getNews();

  $('.flight-row-container').on('click', 'div.flight-text-container',function(){
    addFlightModal($(this).text().split(" ")[1]);
  })

});

const serverURL = "http://192.168.43.104:3000/";

function openToastLogin(){
  M.toast({html: 'Plese Login First!', classes: 'rounded'});
}


function verifyOTP(){

  showLoader();

  console.log($('#userEmail').val());

  var usreCredentialObject  = {
    email:$('#userEmail').val(),
    OTP: $("#userOTP").val()
  };
 

  var settings = {
    "async": true,
    "crossDomain": true,
    "url": serverURL+"verifyOTP",
    "method": "POST",
    "headers": {
      "content-type": "application/x-www-form-urlencoded",
    },
    "data":usreCredentialObject
}
  
$.ajax(settings).done(function (response) {
  removeLoader();
    console.log(response);
    if(response){
        if(response.result == "OTP_VERIFIED"){
          $("#modal2otpFilling").modal('close');
          getFlightData();
        }else{
            M.toast({html:"Wrong OTP entered"},1500);
        }
    }else{
        M.toast({html: "OTP request failed"},1500);
    }
});

}

function showLoader(){
  $('.loader-screen').css('display','block');
}

function removeLoader(){
  $('.loader-screen').css('display','none');
}


function doLogin(){

  console.log(" i am hree");

  showLoader();

  var usreCredentialObject  = {
    email:$('#userEmail').val(),
  };

  var settings = {
      "async": true,
      "crossDomain": true,
      "url": serverURL+"login",
      "method": "POST",
      "headers": {
        "content-type": "application/x-www-form-urlencoded",
      },
      "data":usreCredentialObject
  }
    
  $.ajax(settings).done(function (response) {
    removeLoader();
      console.log(response);
      if(response){
          if(response.result == "OTP_SENT"){
            $("#modal1Login").modal('close');
            $("#modal2otpFilling").modal('open');
          }else{
              M.toast({html:"Wrong Email entered"},1500);
          }
      }else{
          M.toast({html: "Login request failed"},1500);
      }
  });
}


function changeFilter(){
  console.log(" I am here chagne dfuiletrer");
  var companyChecked = $('#checkCompany').prop('checked');
  var modelChecked = $('#checkModel').prop('checked');
  var timeChecked = $('#checkTime').prop('checked');
  var msnChecked = $('#checkMSN').prop('checked');

  console.log(companyChecked);

  var filterArraty =[];
  (companyChecked) ? filterArraty.push('vendor'): '';
  (modelChecked) ? filterArraty.push('model'): ''; 
  (timeChecked) ? filterArraty.push('time'): ''; 
  (msnChecked) ? filterArraty.push('MSN'): ''; 

  console.log(filterArraty);
  var newDataArray = _.orderBy(JSON.parse(localStorage.flightData),filterArraty); // lodash 

  console.log(newDataArray);

  $('.flight-row-container').empty();
            
  var arrayToPush = [];

  newDataArray.forEach((flightSingle)=>{
    var imagetype = (flightSingle.vendor[0].toLowerCase()=='i') ? 'indigo.jpg':'jety.jpg';
    console.log(imagetype);
    var appendString  = '<div class="row" style="margin-bottom:0;"> <div class="col s12"> <div class="card-panel my-flight-card"> <div class="flight-card-image"> <img src="assets/images/'+ imagetype+ '" height="46px"> </div> <div class="flight-text-container"> <div class="flight-number">' +  flightSingle.MSN +'</div> <div class="flight-model"> '+flightSingle.model +' </div> <div class="flight-time"> ' + flightSingle.time +' </div> </div> </div> </div> </div>'
    arrayToPush.push(appendString);
  })

  $('.flight-row-container').append(arrayToPush);
}

function getFlightData(){

  console.log(" i am hree");

  showLoader();


  var settings = {
      "async": true,
      "crossDomain": true,
      "url": serverURL+"getFlights",
      "method": "GET",
      "headers": {
        "content-type": "application/x-www-form-urlencoded",
      }
  }

   
  $.ajax(settings).done(function (response) {
    removeLoader();
      console.log(response);
      if(response){
          if(response.result){
            localStorage.flightData = JSON.stringify(response.result);
            $('.flight-details-screen').css('display','block');
            $('.user-email-name').text($('#userEmail').val());
            $('.landing-screen-container').css('display','none');

            $('.flight-row-container').empty();
            
            var arrayToPush = [];
            response.result.forEach((flightSingle,index)=>{
              console.log("Hey ",flightSingle);
              var imagetype = (flightSingle.vendor[0].toLowerCase()=='i') ? 'indigo.jpg':'jety.jpg';
              console.log(imagetype);
              var appendString  = '<div class="row" id="'+index  +'flighty" style="margin-bottom:0;"> <div class="col s12"> <div class="card-panel my-flight-card"> <div class="flight-card-image"> <img src="assets/images/' + imagetype + ' " height="46px"> </div> <div class="flight-text-container"> <div class="flight-number">' +  flightSingle.MSN +'</div> <div class="flight-model"> '+flightSingle.model +' </div> <div class="flight-time"> ' + flightSingle.time +' </div> </div> </div> </div> </div>'
              arrayToPush.push(appendString);
            })
            
            $('.flight-row-container').append(arrayToPush);
         
          }else{
              M.toast({html:"Flight Fetching Failed"},1500);
          }
      }else{
        M.toast({html:"Flight Fetching Failed"},1500);
      }
  });
}



function getNews(){

  showLoader();

  var settings = {
      "async": true,
      "crossDomain": true,
      "url": serverURL+"getNews",
      "method": "GET",
      "headers": {
        "content-type": "application/x-www-form-urlencoded",
      }
  }

   
  $.ajax(settings).done(function (response) {
    removeLoader();
      console.log(response);
      if(response){
          if(response.result){
            $('.carousel-slider').empty();
            var arrayToPush = [];
            response.result.forEach((headlineSingle,index)=>{
              var appendString = '<div class="carousel-item" href=" #' +index+'"> <div class="row"> <div class="col s12"> <div class="card-panel my-card "> <div class="image-container"> <img src="assets/images/dashboard.png" height="50px"> </div> <div class="text-container"> <div class="text-headline"> ' + headlineSingle.headline + '</div> <div class="text-body"> ' +  headlineSingle.text + '</div> </div> </div> </div> </div> </div>'
              arrayToPush.push(appendString);
            })
            $('.carousel-slider').append(arrayToPush);

            $('.carousel.carousel-slider').carousel({
              fullWidth: true,
              indicators: true
            });

          }else{
              M.toast({html:"Headline Fetching Failed"},1500);
          }
      }else{
        M.toast({html:"Headline Fetching Failed"},1500);
      }
  });
}

function addFlightModal(value){
  console.log(value);

  $('#modalFlight').modal('open');

  var filteredDataModal = _.filter(JSON.parse(localStorage.flightData),{'MSN':Number(value)})

  console.log('Hey ',filteredDataModal);
  $('.flight-data-retail').empty();
  var valuetoAppend = [];
  filteredDataModal.forEach((flightSingle,index)=>{
    var stringValue = '<div class="row"> <div class="col s12 m5"> <div class="card-panel" style="display:flex"> <div class="image-modal-retail" style="width:30%;"> <img src="assets/images/jety.jpg" height="46px"> </div> <div class="data-modal" style="width:70%;display:flex;flex-direction: column;"> <div class="vendor" style="font-size:20px;font-weight:bold;"> ' +flightSingle.vendor + '</div> <div class="container-one" style="display:flex;justify-content: space-between;"> <div class="xyz"> '+ flightSingle.MSN +'</div> <div class="acbd"> '+ flightSingle.flightNo +'</div> </div> <div class="conatiner-two" style="display:flex;justify-content: space-between;"> <div class="fuel-consum"> '+ flightSingle.FCleftWing +'</div> <div class="atmosphreic-pressure"> ' + flightSingle.maxAlt +' </div> <div> '+ flightSingle.airport +' </div> </div> <div> <a class="waves-effect waves-light btn" style="color:white;">Reminder</a> </div> </div> </div> </div> </div>'
    valuetoAppend.push(stringValue);
  })

  $('.flight-data-retail').append(valuetoAppend);

}

function openSearchModal(){
  $('#searchModal').modal('open');
}

function searchFunction(){


  var usreCredentialObject ={
    MSN:$("#MSN_number").val(),
    flightNo:$('#Flight_query_number').val(),
    airport: $('#userAirport').val()
  }

  var settings = {
    "async": true,
    "crossDomain": true,
    "url": serverURL+"getFilteredData",
    "method": "POST",
    "headers": {
      "content-type": "application/x-www-form-urlencoded",
    },
    data:usreCredentialObject
}

 
$.ajax(settings).done(function (response) {
  removeLoader();
    console.log(response);
    if(response){
        if(response.result){

          console.log(response.result);
          // var arrayToPush = [];
          // response.result.forEach((headlineSingle,index)=>{
          //   var appendString = '<div class="carousel-item" href=" #' +index+'"> <div class="row"> <div class="col s12"> <div class="card-panel my-card "> <div class="image-container"> <img src="assets/images/dashboard.png" height="50px"> </div> <div class="text-container"> <div class="text-headline"> ' + headlineSingle.headline + '</div> <div class="text-body"> ' +  headlineSingle.text + '</div> </div> </div> </div> </div> </div>'
          //   arrayToPush.push(appendString);
          // })
          // $('.carousel-slider').append(arrayToPush);

          // $('.carousel.carousel-slider').carousel({
          //   fullWidth: true,
          //   indicators: true
          // });

        }else{
            M.toast({html:"Headline Fetching Failed"},1500);
        }
    }else{
      M.toast({html:"Headline Fetching Failed"},1500);
    }
});



}


